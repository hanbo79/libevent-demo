﻿// libeventDemo.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//
//#include "event2/event-config.h"
#ifdef _WIN32
#include <winsock2.h>
#pragma comment(lib,"ws2_32.lib")
#else
#include <unistd.h>
#endif

#include "event2/event.h"
#include "event2/util.h"
#include "event2/event_compat.h"
#include "event2/event_struct.h"
#include "event2/bufferevent.h"
#include "event2/thread.h"

#include <stdint.h>
#include <thread>

typedef struct _eventPerIo
{
	struct event* pevent;

	int interval;

	std::chrono::steady_clock::time_point m_startT;
}eventPerIo;

int g_timecbcount = 0;
int  g_timecbMax = 0;
int g_settimecount = 0;


static void time_cb(evutil_socket_t fd, short event, void* arg)
{
	if (arg) {
		eventPerIo* pio = (eventPerIo*)arg;
		auto endtime = std::chrono::steady_clock::now();
		int diff = (int)std::chrono::duration_cast<std::chrono::milliseconds>(endtime - pio->m_startT).count();
		/*if (diff > 1015) 
		{
			struct timeval tv;
			evutil_gettimeofday(&tv, nullptr);
			printf("\ntimer event:%hd,%u:%u", event, tv.tv_sec, tv.tv_usec);
			printf("\nevent:interval =%d,diff:%d", pio->interval, diff);
		}*/
		if (diff > g_timecbMax) {
			g_timecbMax = diff;
		}
		g_timecbcount++;
		event_free(pio->pevent);
		delete pio;
	}
	else {
		struct timeval tv;
		evutil_gettimeofday(&tv, nullptr);
		printf("\ntimer,%u:%u: setcount:%d,count:%d,diffmax:%d", tv.tv_sec, tv.tv_usec, g_settimecount, g_timecbcount, g_timecbMax);
		g_timecbcount = 0;
		g_timecbMax = 0;
		g_settimecount = 0;

	}
}

#include <mutex>
#include <deque>
struct event_base* base;


static UINT64 setTimer( int intervalUs)
{
	g_settimecount++;
	eventPerIo* pIo = new eventPerIo;
	pIo->interval = intervalUs;
	pIo->m_startT = std::chrono::steady_clock::now();

	struct event* timer2 = event_new(base, -1, 0, time_cb, (void*)pIo); //event_self_cbarg()
	pIo->pevent = timer2;

	timeval escape_time;
	escape_time.tv_sec = 1;
	escape_time.tv_usec = 0;// pIo->interval;
	event_add(timer2, &escape_time);

	return 0;
}

static void thread_fun(void *agv)
{
	int* pagv = (int*)agv;
	printf("subthread run :%d", *pagv);
#if 1
	while (1) 
	{
		for (int i = 0; i < 1000; i++) {
			setTimer(1000000);// rand() % 1000000);
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
#endif
#if 0
	while (1) {
		setTimer(1, 1000000);
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}
#endif
	
	printf("\nsubthread end :%d", *pagv);
}
#include <ctime>
int timer_main2()
{

#ifdef _WIN32
	WORD wVersionRequested;
	WSADATA wsaData;

	wVersionRequested = MAKEWORD(2, 2);

	(void)WSAStartup(wVersionRequested, &wsaData);
#endif
#ifdef WIN32
	evthread_use_windows_threads();
#else
	evthread_use_pthreads();
#endif
	
	base = event_base_new();

	static timeval t2;
	t2.tv_sec = 1;
	t2.tv_usec = 0;
	event* timer2 = event_new(base, -1, EV_PERSIST, time_cb, 0);
	event_add(timer2, &t2);

	int* pagb = new int(22);
	std::thread* subthread = new std::thread(thread_fun, pagb);

/* Dispatch */
	/*while (1) {
		int i = event_base_dispatch(base);

		printf("\nevent_base_dispatch=%d\n",i);
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}*/
	event_base_loop(base, EVLOOP_NO_EXIT_ON_EMPTY);
	
	return 0;
}
