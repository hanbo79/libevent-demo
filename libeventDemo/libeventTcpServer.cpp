// libeventDemo.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//
//#include "event2/event-config.h"
#ifdef _WIN32
#include <winsock2.h>
#pragma comment(lib,"ws2_32.lib")
#else
#include <unistd.h>
#endif

#include "event2/event.h"
#include "event2/util.h"
#include "event2/event_compat.h"
#include "event2/event_struct.h"
#include "event2/bufferevent.h"
#include "event2/thread.h"

#include <event2/buffer.h>
#include <event2/listener.h>
#include <thread>

static void conn_writecb(struct bufferevent* bev, void* user_data)
{
	struct evbuffer* output = bufferevent_get_output(bev);
	if (evbuffer_get_length(output) == 0) {
		printf("flushed answer finish\n");
		//bufferevent_free(bev);
	}
	else {
		printf("send pending\n");
	}
}
static void conn_readcb(struct bufferevent* bev, void* user_data)
{
#if 1
	//接收缓存测试，不读取，看readcb
	evbuffer *rbuffer = bufferevent_get_input(bev);
	int rlen = evbuffer_get_length(rbuffer);
	printf("recv,len:%d\n", rlen);
#else
	char buf[128] = { 0 };
	int readlen = bufferevent_read(bev, buf, 127);
	printf("recv,len:%d,data:%s\n", readlen, buf);

	char sendbuf[256] = { 0 };
	sprintf_s(sendbuf, sizeof(sendbuf), " ack %s", buf);
	bufferevent_write(bev, sendbuf, strlen(sendbuf) + 1);
#endif
}
static void conn_eventcb(struct bufferevent* bev, short events, void* user_data)
{
	printf("conn_eventcb events:%x\n", events);
	if (events & BEV_EVENT_EOF) {
		printf("Connection closed.\n");
	}
	else if (events & BEV_EVENT_ERROR) {
		printf("Got an error on the connection: %s\n",
			strerror(errno));/*XXX win32*/
	}
	
	/* None of the other events can happen here, since we haven't enabled
	 * timeouts */
	bufferevent_free(bev);
}

//服务端多线程发送
void server_send_thread(void* client) {
	struct bufferevent* pev = (struct bufferevent*)client;
	for (int i = 0; i < 10000; i++) {
		bufferevent_write(pev, "i am server", strlen("i am server"));
	}
}

static void listener_cb(struct evconnlistener* listener, evutil_socket_t fd,
	struct sockaddr* sa, int socklen, void* user_data)
{
	struct event_base* base =(struct event_base *)user_data;
	struct bufferevent* bev;

	bev = bufferevent_socket_new(base, fd, BEV_OPT_CLOSE_ON_FREE);
	if (!bev) {
		fprintf(stderr, "Error constructing bufferevent!");
		//event_base_loopbreak(base);
		evutil_closesocket(fd);
		return;
	}

	printf("recv connect 2\n");

	/*std::this_thread::sleep_for(std::chrono::milliseconds(2000));
	printf("recv connect2\n");*/

	bufferevent_setcb(bev, conn_readcb, conn_writecb, conn_eventcb, NULL);
	bufferevent_enable(bev, EV_WRITE| EV_READ);
	//bufferevent_disable(bev, EV_WRITE|EV_READ);

	//bufferevent_write(bev, "i am server", strlen("i am server"));
	//std::thread(server_send_thread, bev).detach();  //接收连接后启动线程发送数据
}

int tcpServer_main()
{
//#ifdef _WIN32
	WSADATA wsa_data;
	WSAStartup(0x0201, &wsa_data);

	evthread_use_windows_threads();
//#endif

	struct evconnlistener* listener;
	struct sockaddr_in sin = { 0 };

	struct event_base *base = event_base_new();
	if (!base) {
		fprintf(stderr, "Could not initialize libevent!\n");
		return 1;
	}

	sin.sin_family = AF_INET;
	sin.sin_port = htons(8001);

	listener = evconnlistener_new_bind(base, listener_cb, (void*)base,
		LEV_OPT_REUSEABLE | LEV_OPT_CLOSE_ON_FREE, -1,
		(struct sockaddr*)&sin,
		sizeof(sin));

	if (!listener) {
		fprintf(stderr, "Could not create a listener!\n");
		return 1;
	}

	printf("start waiting 8000.\n");

	event_base_loop(base, EVLOOP_NO_EXIT_ON_EMPTY);
/* Dispatch */
	/*while (1) {
		int i = event_base_dispatch(base);

		printf("\nevent_base_dispatch=%d\n",i);
		std::this_thread::sleep_for(std::chrono::seconds(5));
	}*/
	printf("main exit!!!!!!!!!!!!\n");
	getchar();
	return 0;
}
